<!doctype html>
<html lang="en">
	<?php include_once("includes/header.php");?>
		<div class="container vertical-align">
			<div class="row justify-content-md-center">
				<form action='login.php' method='post'>
					<div class="form-group">
						Endereco de email
						<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" name='email'>
		
					</div>
					<div class="form-group">
						Senha
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha" name='senha'>
					</div>
	
					<input type="submit" class="btn btn-primary my-2" value="Entrar">
					<div class="form-group">		
						<a href="cadastro.php?ator=cliente">Quero ser cliente do Serviço Fácil </a>
					</div>		
					<div class="form-group">
						<a href="cadastro.php?ator=prestador">Sou profissional e quero me candidatar a prestar serviço</a>
					</div>
				</form>

			</div>
		</div>
		<?php include_once("includes/footer.php");?>
	</body>
</html>
