<?php
    session_start();
    if(!isset( $_SESSION["servico"])){
        $_SESSION["servico"] [0] = "Bombeiro hidraulico";
        $_SESSION["servico"] [1] = "Eletricista";
        $_SESSION["servico"] [2] = "Chaveiro";
        $_SESSION["servico"] [3] = "Marceneiro";
    }
    if(!isset( $_SESSION["catalogo"])){
        $_SESSION["catalogo"] [0] = "Vazamento em torneira";
        $_SESSION["catalogo"] [1] = "Vazamento em descarga";
        $_SESSION["catalogo"] [2] = "Vazamento no teto";
    }
    
    
    function setServico($novoservico){
        $_SESSION["servico"][] = $novoservico;
    }
    function setCatalogo($novocatalogo){
        $_SESSION["catalogo"][] = $novocatalogo;
    
    }
    function getServico(){
        return $_SESSION["servico"] ;
    }
    function getCategoria(){
        return $_SESSION["catalogo"] ;
    }
?>