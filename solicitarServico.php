<?php
    
    include_once("catalogoMetodo.php");
    $servico = getServico();
    $catalogo = getCategoria();
    
    if(!$servico || !$catalogo){
        $servico[0] = 'Nenhum';
        $catalogo[0] = 'Nenhum';
    }

    
?>
<!doctype html>
<html lang="en">
	<?php include_once("includes/header.php");?>
		<div class="container vertical-align">
			<div class="row justify-content-md-center">
				<form action='#' method='post'>
				    
					<div class="form-group">
					    <p>Escolha o serviço desejado</p>
						<p>Tipo de serviço</p>
	    				<select class="form-control" id="exampleFormControlSelect1" name='servico'>
                            <?php
                                foreach($servico as $serv){
                                    echo "<option value='".$serv."'>".$serv." </option>";
                                }
                            ?>
	   					 </select>
					</div>
					<div class="form-group">
						<select multiple class="form-control" id="exampleFormControlSelect2" name='catalogo'>
                            <?php
                                foreach($catalogo as $cata){
                                    echo "<option value='".$cata."'>".$cata." </option>";
                                }
                            ?>
						</select>
					</div>
					
					<input type="submit" class="btn btn-primary" value="Escolher">				
				</form>

			</div>
		</div>
		<?php include_once("includes/footer.php");?>
	</body>
</html>